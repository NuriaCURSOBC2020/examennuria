table 90100 "TCN_EntradaDatos"
{
    Caption = 'TCN_EntradaDatos';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Texto01; Text[100])
        {
            Caption = 'Texto01';
            DataClassification = ToBeClassified;
        }
        field(2; Texto02; Text[100])
        {
            Caption = 'Texto02';
            DataClassification = ToBeClassified;
        }
        field(3; Num01; Decimal)
        {
            Caption = 'Num01';
            DataClassification = ToBeClassified;
        }
        field(4; Num02; Decimal)
        {
            Caption = 'Num02';
            DataClassification = ToBeClassified;
        }
        field(5; Date01; Date)
        {
            Caption = 'Date01';
            DataClassification = ToBeClassified;
        }
        field(6; Date02; Date)
        {
            Caption = 'Date02';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; Texto01)
        {
            Clustered = true;
        }
    }

}
