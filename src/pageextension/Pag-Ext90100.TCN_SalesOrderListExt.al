pageextension 90100 "TCN_SalesOrderListExt" extends "Sales Order List" //MyTargetPageId
{
    layout
    {

    }

    actions
    {
        addlast(Creation)
        {
            action(ImportarDatos)
            {
                trigger OnAction()
                var
                    culImportarFicheros: Codeunit TCN_ImportarFicheros;
                begin
                    culImportarFicheros.ImportarFicheroF();
                end;


            }
        }



    }
}