codeunit 90100 "TCN_ImportarFicheros"
{
    Permissions = tabledata "Sales Header" = rimd, tabledata "Sales Line" = rimd;

    procedure ImportarFicheroF()
    var
        rlTempBlogTMP: Record TempBlob temporary;
        rlCustomer: Record Customer;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        plFichaEntradasDatos: Page TCN_FichaEntradasDatos;
        rlCommentLineTMP: Record "Comment Line" temporary;
        xlInStream: InStream;
        xlFichero: Text;
        xlLinea: text;
        xlNumLinea: Integer;
        xlCaption: Text;
        xlNumCliente: Text;
        xlEtiSeparador: Text;
        xlNumClienteCode: code[20];
        xlNombreSeparador: Text[1];
        xlPrimerCampLin: Text;
        xlEtiqError: Label 'No se pudo subir fichero';
        xlEtiqErrorSep: Label 'Tiene que poner un separador';
        xlEtiqErrorCli: Label 'Tiene que poner un cliente valido';
        xlNumLinPed: integer;


    begin
        xlCaption := 'Nº Cliente:';
        xlEtiSeparador := 'Separador fichero:';
        plFichaEntradasDatos.SetEntradaDatosF(xlCaption, xlNumCliente);
        plFichaEntradasDatos.SetEntradaDatosF(xlEtiSeparador, xlNombreSeparador);
        if plFichaEntradasDatos.RunModal() in [action::LookupOK, action::OK] then begin
            plFichaEntradasDatos.GetEntradaDatosF(xlNumCliente);
            plFichaEntradasDatos.GetEntradaDatosF(xlNombreSeparador);
            if Evaluate(xlNumClienteCode, xlNumCliente) then begin
                if rlCustomer.Get(xlNumClienteCode) then begin
                    if xlNombreSeparador <> '' then begin

                        rlTempBlogTMP.Blob.CreateInStream(xlInStream, TextEncoding::Windows);
                        if UploadIntoStream('Seleccione fichero', '', 'Archivos de Texto (*.txt, *.csv)|*.txt;*.csv| Todos los archivos(*.*)|*.*',
                                                                    xlFichero, xlInStream) then begin

                            while not xlInStream.EOS do begin
                                xlInStream.ReadText(xlLinea);
                                xlNumLinea += 1;

                                rlSalesHeader.Init();
                                rlSalesHeader.Validate("Document Type", rlSalesHeader."Document Type"::Order);

                                xlPrimerCampLin := CopyStr(xlLinea, 1, 1);
                                if UpperCase(xlPrimerCampLin) = 'C' then begin
                                    if CabeceraF(xlNombreSeparador, xlLinea, rlSalesHeader) then begin
                                        rlSalesHeader.Insert(true);
                                        rlSalesHeader.Validate("No.");
                                        xlNumLinPed := 0;
                                        rlCommentLineTMP.Init();
                                        rlCommentLineTMP."Line No." := xlNumLinea;
                                        rlCommentLineTMP.Comment := CopyStr(StrSubstNo('Linea %1, 1º campo %2, posting date %3,document date %4,external document %5'
                                                        , xlNumLinea, xlPrimerCampLin, rlSalesHeader."Posting Date", rlSalesHeader."Document Date", rlSalesHeader."External Document No."),
                                                        1, MaxStrLen(rlCommentLineTMP.Comment));
                                        rlCommentLineTMP.Insert(false);
                                        page.Run(0, rlCommentLineTMP);
                                    end else begin
                                        GuardarErrorF(rlCommentLineTMP, xlNumLinea);
                                    end;
                                end else begin
                                    rlSalesLine.Init();
                                    rlSalesLine.Validate("Document Type", rlSalesLine."Document Type"::Order);
                                    rlSalesLine.Validate("Sell-to Customer No.", xlNumClienteCode);
                                    rlSalesLine.Validate("Document No.", rlSalesHeader."No.");
                                    xlNumLinPed += 10;
                                    rlSalesLine.Validate("Line No.", xlNumLinPed);
                                    if UpperCase(xlPrimerCampLin) = 'L' then begin
                                        if LineasF(xlNombreSeparador, xlLinea, rlSalesLine, xlNumLinPed) then begin
                                            rlSalesLine.Insert(true);
                                        end else begin
                                            rlCommentLineTMP.Init();
                                            rlCommentLineTMP."Line No." := xlNumLinea;
                                            rlCommentLineTMP.Comment := CopyStr(StrSubstNo('Linea %1, error %2', xlNumLinea, GetLastErrorText), 1, MaxStrLen(rlCommentLineTMP.Comment));
                                            rlCommentLineTMP.Insert(false);
                                            // GuardarErrorF(rlCommentLineTMP, xlNumLinea);
                                        end;
                                    end;
                                end;
                            end;
                        end else begin
                            Error(xlEtiqError);
                        end;
                    end else begin
                        Error(xlEtiqErrorSep);
                    end;
                end else begin
                    Error(xlEtiqErrorCli);
                end;
                if rlCommentLineTMP."Line No." = 0 then begin
                    Message('Proceso finalizado');

                end else begin
                    page.Run(0, rlCommentLineTMP);
                end;
            end;
        end;
    end;


    local procedure FormatoFechaF(prFecha: text) xDate: Date

    var
        xlDia: Integer;
        xlMes: Integer;
        xlAnio: Integer;

    begin
        Evaluate(xlDia, CopyStr(prFecha, 1, 2));
        Evaluate(xlMes, CopyStr(prFecha, 3, 2));
        Evaluate(xlAnio, CopyStr(prFecha, 5, 4));
        xDate := DMY2Date(xlDia, xlMes, xlAnio);
    end;

    [TryFunction]
    local procedure CabeceraF(pSeparador: text[1]; pLinea: text; var prSalesHeader: Record "Sales Header")
    var
        xlCampo: text;
        xlNumCampo: Integer;

    begin
        xlNumCampo := 1;
        foreach xlCampo in plinea.Split(pSeparador) do begin

            case xlNumCampo of
                2:
                    begin
                        prSalesHeader.Validate("Posting Date", FormatoFechaF(xlCampo));
                    end;
                3:
                    begin
                        prSalesHeader.Validate("Document Date", FormatoFechaF(xlCampo));
                    end;
                4:
                    begin
                        prSalesHeader.Validate("External Document No.", xlCampo);
                    end;
            end;
            xlNumCampo += 1;
        end;
    end;

    [TryFunction]
    local procedure LineasF(pSeparador: text[1]; pLinea: text; var prSalesLine: Record "Sales Line"; pNumLinPed: integer)
    var
        xlCampo: text;
        xlNumCampo: Integer;
        xlErrorTipLin: Label 'Error tipo linea';
        xlCantidad: Decimal;
        xlPrecioVta: Decimal;
        xlDto: decimal;
        // xlNumCode: Code[20];

    begin
        xlNumCampo := 1;
        foreach xlCampo in plinea.Split(pSeparador) do begin

            case xlNumCampo of
                2:
                    begin
                        case UpperCase(xlCampo) of
                            'C':
                                begin

                                    prSalesLine.Validate("Type", prSalesLine.Type::"G/L Account");
                                end;
                            'P':
                                begin

                                    prSalesLine.Validate("Type", prSalesLine.Type::Item);
                                end;
                            else
                                Error(xlErrorTipLin);

                        end;
                    end;
                3:
                    begin

                        prSalesLine.Validate("No.", xlCampo);
                    end;

                4:
                    begin
                        prSalesLine.Validate(Description, xlCampo);
                    end;
                5:
                    begin
                        Evaluate(xlCantidad, xlCampo);
                        xlCantidad /= 100;
                        prSalesLine.Validate(Quantity, xlCantidad);

                    end;
                6:
                    begin
                        Evaluate(xlPrecioVta, xlCampo);
                        xlPrecioVta /= 100;
                        prSalesLine.Validate("Unit Price", xlPrecioVta);

                    end;
                7:
                    begin
                        Evaluate(xlDto, xlCampo);
                        xlDto /= 100;
                        prSalesLine.Validate("Line Discount %", xlDto);

                    end;
            end;
            xlNumCampo += 1;
        end;

    end;

    local procedure GuardarErrorF(pCommentLineTMP: Record "Comment Line" temporary; pNumLinea: Integer)
    begin
        pCommentLineTMP.Init();
        pCommentLineTMP."Line No." := pNumLinea;
        pCommentLineTMP.Comment := CopyStr(StrSubstNo('Linea %1, error %2', pNumLinea, GetLastErrorText), 1, MaxStrLen(pCommentLineTMP.Comment));
        pCommentLineTMP.Insert(false);
    end;
}