page 90100 "TCN_FichaEntradasDatos"
{
    Caption = 'Entrada de datos';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = TCN_EntradaDatos;

    layout
    {
        area(Content)
        {
            group(Datos)
            {
                field(Texto01; xArrayDatosTexto[1])
                {
                    ApplicationArea = All;
                    CaptionClass = StrSubstNo('3, %1', xArrayCaption[1]);
                    Visible = xVisible1;

                }
                field(Texto02; xArrayDatosTexto[2])
                {
                    ApplicationArea = All;
                    CaptionClass = StrSubstNo('3, %1', xArrayCaption[2]);
                    Visible = xVisible2;
                }

                field(Num01; xArrayDatosNum[1])
                {
                    ApplicationArea = All;
                    CaptionClass = StrSubstNo('3, %1', xArrayCaption[3]);
                    Visible = xVisible3;
                }
                field(Num02; xArrayDatosNum[2])
                {
                    ApplicationArea = All;
                    CaptionClass = StrSubstNo('3, %1', xArrayCaption[4]);
                    Visible = xVisible4;
                }
                field(Date01; xArrayDatosDate[1])
                {
                    ApplicationArea = All;
                    CaptionClass = StrSubstNo('3, %1', xArrayCaption[5]);
                    Visible = xVisible5;
                }
                field(Date02; xArrayDatosDate[2])
                {
                    ApplicationArea = All;
                    CaptionClass = StrSubstNo('3, %1', xArrayCaption[6]);
                    Visible = xVisible6;

                }

            }
        }
    }
    trigger OnOpenPage()
    var

    begin


    end;

    procedure SetEntradaDatosF(pCaption: Text[50]; pTexto: text)
    var

    begin
        xContadorTexSet += 1;
        if xContadorTexSet <= 2 then begin
            xArrayCaption[xContadorTexSet] := pCaption;
            xArrayDatosTexto[xContadorTexSet] := pTexto;
            if xContadorTexSet = 1 then begin
                xVisible1 := True;
            end;
            if xContadorTexSet = 2 then begin
                xVisible2 := True;
            end;
            if xContadorTexSet >= 3 then begin
                xContadorTexSet := 0;
            end;


        end;
    end;

    procedure SetEntradaDatosF(pCaption: Text[50]; pNumero: Decimal)
    var

    begin
        xContadorNumSet += 1;
        if xContadorNumSet <= 2 then begin
            xArrayCaption[xContadorNumSet + 2] := pCaption;
            xArrayDatosNum[xContadorNumSet] := pNumero;
            if xContadorNumSet = 1 then begin
                xVisible3 := True;
            end;
            if xContadorNumSet = 2 then begin
                xVisible4 := True;
            end;
            if xContadorNumSet >= 3 then begin
                xContadorNumSet := 0;
            end;
        end;
    end;

    procedure SetEntradaDatosF(pCaption: Text[50]; pDate: date)
    var

    begin
        xContadorDateSet += 1;
        if xContadorDateSet <= 2 then begin
            xArrayCaption[xContadorDateSet + 4] := pCaption;
            xArrayDatosDate[xContadorDateSet] := pDate;
            if xContadorDateSet = 1 then begin
                xVisible5 := True;
            end;
            if xContadorDateSet = 2 then begin
                xVisible6 := True;
            end;
            if xContadorDateSet >= 3 then begin
                xContadorDateSet := 0;
            end;
        end;
    end;

    procedure GetEntradaDatosF(var pTexto: text[100])
    var

    begin
        xContadorTexGet += 1;
        if xContadorTexGet <= 2 then begin
            pTexto := xArrayDatosTexto[xContadorTexGet];
            if xContadorTexGet >= 3 then begin
                xContadorTexGet := 0;
            end;
        end;
    end;

    procedure GetEntradaDatosF(var pNumero: Decimal)
    var

    begin
        xContadorNumGet += 1;
        if xContadorNumGet <= 2 then begin
            pNumero := xArrayDatosNum[xContadorNumGet];
            if xContadorNumGet >= 3 then begin
                xContadorNumGet := 0;
            end;
        end;
    end;

    procedure GetEntradaDatosF(var pDate: date)
    var


    begin
        xContadorDateGet += 1;
        if xContadorDateGet <= 2 then begin
            pdate := xArrayDatosDate[xContadorDateGet];
            if xContadorDateGet >= 3 then begin
                xContadorDateGet := 0;
            end;

        end;
    end;



    var


        xArrayCaption: array[6] of text;
        xArrayDatosDate: array[2] of Date;
        xArrayDatosNum: array[2] of Decimal;
        xArrayDatosTexto: array[2] of Text;
        xContadorTexSet: integer;
        xContadorNumSet: integer;
        xContadorDateSet: integer;
        xContadorTexGet: integer;
        xContadorNumGet: integer;
        xContadorDateGet: integer;
        xVisible1: Boolean;
        xVisible2: Boolean;
        xVisible3: Boolean;
        xVisible4: Boolean;
        xVisible5: Boolean;
        xVisible6: Boolean;

    trigger OnInit()
    begin
        xContadorTexSet := 0;
        xContadorNumSet := 0;
        xContadorDateSet := 0;
        xContadorTexGet := 0;
        xContadorNumGet := 0;
        xContadorDateGet := 0;
        xVisible1 := false;
        xVisible2 := false;
        xVisible3 := false;
        xVisible4 := false;
        xVisible5 := false;
        xVisible6 := false;

    end;
}