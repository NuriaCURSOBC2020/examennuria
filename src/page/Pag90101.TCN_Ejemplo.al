page 90101 "TCN_Ejemplo"
{
    Caption = 'Por favor, funciona!!!';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = Integer;



    actions
    {
        area(Processing)
        {
            action(Pruebas)
            {
                ApplicationArea = All;


                trigger OnAction()
                var
                    plFichaEntradasDatos: Page TCN_FichaEntradasDatos;
                    xlFiltro: Text;
                    xlCodigo: Text;
                    xlNum: decimal;
                    xlDate: Date;

                begin
                    // xlNum := 23.5;
                    // xltexto := 'hola';
                    // xltexto2 := '¿Cómo estas?';
                    // xlDate := Today;
                    // plFichaEntradasDatos.SetEntradaDatosF(xltexto, xlNum);
                    // plFichaEntradasDatos.SetEntradaDatosF(xltexto, xltexto2);
                    // plFichaEntradasDatos.SetEntradaDatosF(xltexto, xlDate);
                    plFichaEntradasDatos.SetEntradaDatosF('Dime una cadena de filtros:', xlFiltro);
                    plFichaEntradasDatos.SetEntradaDatosF('Dime un código cliente:', xlCodigo);
                    if plFichaEntradasDatos.RunModal() in [action::LookupOK, action::OK] then begin

                        plFichaEntradasDatos.GetEntradaDatosF(xlFiltro);
                        plFichaEntradasDatos.GetEntradaDatosF(xlCodigo);
                        if EstaDentroFiltroF(xlFiltro, xlCodigo) then begin
                            Message('El cliente está dentro del rango de búsqueda');
                        end else begin
                            Error('El cliente no está dentro del rango de búsqueda')
                        end;
                    end;
                end;
            }
        }
    }
    local procedure EstaDentroFiltroF(pTexto: text; pCodigo: code[20]) xSalida: Boolean
    var
        rlTempCustomer: Record Customer temporary;
    begin
        rlTempCustomer.Init();
        // rlTempCustomer.SetCurrentKey("No.");
        rlTempCustomer.SetFilter("No.", pTexto);
        rlTempCustomer.SetRange("No.", pCodigo);
        if rlTempCustomer.FindFirst() then begin
            xSalida := true;
        end else begin
            xSalida := False;
        end;

    end;
}